<?php include 'connect.php'; ?>

<!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Bogor Smart Trans - Angkot</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    </head>

    <body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top ">
        <div class="container d-flex align-items-center">

        <h1 class="logo me-auto"><a href="index.php">BST</a></h1>

        <nav id="navbar" class="navbar">
            <ul>
            <li><a class="nav-link scrollto active" href="#hero-kendaraan">Beranda</a></li>
            <li><a class="nav-link scrollto" href="#about">Tentang</a></li>
            <li><a class="nav-link scrollto" href="#services">Layanan</a></li>
            <li> 
                <?php if (empty($_SESSION['username'])) { ?>
                <a href="formLogin.php"> Login </a>
                <?php } else { ?>
                <a href="logout.php">Logout</a>
                <?php } ?>
            </li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero-kendaraan" class="d-flex align-items-center">

        <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
            <h1>Angkutan Kota</h1>
            </div>
        </div>
        </div>

    </section>
    <!-- End Hero -->

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
        <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Tentang Angkot</h2>
        </div>

        <div class="row content">
            <div class="align-items-center text-center">
            <p>
                Dalam transportasi di Indonesia, angkutan kota atau biasa disingkat angkot adalah sebuah transportasi umum jenis taksi bersama dengan rute yang sudah ditentukan. Tidak seperti bus yang mempunyai halte bus sebagai tempat perhentian yang sudah ditentukan, angkutan kota dapat berhenti untuk menaikkan atau menurunkan penumpang di mana saja.
            </p>
            </div>
            <div class="align-items-center text-center">
            <p><br>
                Keberadaan angkot dimulai pada tahun 1943 ketika Indonesia masih dijajah oleh Jepang. Jenis transportasi ini dimaksudkan sebagai cara bepergian dengan kendaraan bermotor. Pada tahun 1946, angkot menjadi bagian dari DAMRI (Djawatan Angkoetan Motor Repoeblik Indonesia) sebagai angkutan umum. Angkot atau mikrolet menjadi sangat populer karena mampu melakukan perjalanan dengan kendaraan yang relatif kecil hingga 10 penumpang di dalamnya. Mikrolet jauh lebih kecil daripada bus, sehingga lebih mudah bagi mereka untuk bepergian di jalan-jalan Jakarta tanpa menyebabkan kemacetan lalu lintas.
            </p>
            </div>
        </div>

        </div>
    </section><!-- End About Us Section -->

    <!-- ======= Service Section ======= -->
    <section id="services" class="services">
        <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Layanan Angkot</h2>
        </div>
        <?php if (!empty($_SESSION['username'])) { ?>
        <a href="formAngkot.php" class="tambah btn btn-sm btn-success mb-3">Tambah</a>
        <?php } ?>
        <table class="table">
            <thead class="table-light">
            <tr>
                <th>No.Angkot</th>
                <th>Trayek</th>
                <th>Rute</th>
                <?php if (!empty($_SESSION['username'])) { ?>
                <th>Aksi</th>
                <?php } ?>
                
            </tr>
            </thead>
            <tbody>
            <?php
                $sql = 'SELECT * FROM angkot ORDER BY no_angkot asc';

                $query = mysqli_query($conn, $sql);
                while ($row = mysqli_fetch_object($query)) {
            ?>
            <tr>
                <td><?php echo $row->no_angkot; ?></td>
                <td><?php echo $row->trayek; ?></td>
                <td><?php echo $row->rute; ?></td>
                <?php if (!empty($_SESSION['username'])) { ?>
                <td> 
                <a href="deleteAngkot.php?id_angkot=<?php echo $row->id_angkot; ?> " class="btn btn-sm btn-danger" 
                onclick="return confirm('apakah anda yakin ingin menghapus data?');">HAPUS</a>
                <a href="formAngkot.php?id_angkot=<?php echo $row->id_angkot; ?> " class="btn btn-sm btn-warning">UBAH</a>
                </td>
                <?php } ?>
                
            </tr>
            <?php
                } if (mysqli_num_rows($query) == 0) {
                    echo '<tr><td colspan="5" class="text-center">Tidak ada data.</td></tr>';
                }
            ?>
            </tbody>
        </table>

        </div>
    </section><!-- End About Us Section -->

        

    <!-- ======= Footer ======= -->
    <footer id="footer">

        <div class="footer-top">
        <div class="container">
            <div class="row">

            <div class="col-lg-3 col-md-6 footer-contact">
                <h3>BST</h3>
                <p>
                Bogor Regency<br>
                Indonesia<br><br>
                <strong>Telepon:</strong> 1500411<br>
                <strong>Email:</strong> bogor@gmail.com<br>
                </p>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
                <h4>Sosial Media Kami</h4>
                <div class="social-links mt-3">
                <a href="https://twitter.com/PemkotaBogor?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target='_blank' class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="https://id-id.facebook.com/PemerintahKotaBogor/" target='_blank' class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="https://www.instagram.com/pemkotbogor/?hl=en" target='_blank' class="instagram"><i class="bx bxl-instagram"></i></a>
                </div>
            </div>

            </div>
        </div>
        </div>

        <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>BST</span></strong>. All Rights Reserved
        </div>
        </div>
    </footer><!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

    </body>

</html>