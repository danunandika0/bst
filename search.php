<?php include 'connect.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Bogor Smart Trans - Rute</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- datatables.net -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.2/css/buttons.dataTables.min.css">
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>


</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="index.php">BST</a></h1>
      <!-- .navbar -->

      <!-- <nav id="navbar" class="navbar">
        <ul>
          <li>
          <div class="input-group">
            <form action="search.php" method="get" class="d-flex">
              <input type="text" class="form-control rounded" placeholder="Cari Rute" name="cari" autocomplete='off' />
              <button type="submit" class='btn'>Cari</button>
            </form>
          </div>
          </li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav> -->

    </div>
  </header><!-- End Header -->

  <section id="cover" class="align-items-center">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Layanan Angkot</h2>
      </div>
      <table class="table table-striped table-bordered">
        <thead class="table-light">
          <tr>
            <th>No.Angkot</th>
            <th>Trayek</th>
            <th>Rute</th>

          </tr>
        </thead>
        <tbody>
          <?php
          // $sql = "SELECT * FROM `angkot` order by no_angkot asc";
          $sql = "SELECT * from angkot";
          // if (isset($_GET['cari'])) {
          //   $keyword = $_GET['cari'];
          //   $sql = "SELECT * from angkot where rute like '%$keyword%'";
          // }

          $query = mysqli_query($conn, $sql);
          while ($row = mysqli_fetch_object($query)) {
          ?>
            <tr>
              <td><?php echo $row->no_angkot; ?></td>
              <td><?php echo $row->trayek; ?></td>
              <td><?php echo $row->rute; ?></td>

            </tr>
          <?php
          }
          if (mysqli_num_rows($query) == 0) {
            echo '<tr><td colspan="5" class="text-center">Tidak ada data.</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>

    <section id="cover" class="align-items-center">
    <div class="container" data-aos="fade-up">
      <div class="section-title">
        <h2>Layanan Trans Pakuan</h2>
      </div>
      <table class="table table-striped table-bordered">
        <thead class="table-light">
          <tr>
            <th>Koridor</th>
            <th>Trayek</th>
            <th>Rute</th>
          </tr>
        </thead>
        <tbody>
        <?php
            // $sql = 'SELECT * FROM biskita ORDER BY koridor asc';
            $sql = "SELECT * from biskita ";

            // if (isset($_GET['cari'])) {
            //   $keyword = $_GET['cari'];
            //   $sql = "SELECT * from biskita where rute like '%$keyword%'";
            // }

            $query = mysqli_query($conn, $sql);
            while ($row = mysqli_fetch_object($query)) {
        ?>
          <tr>
            <td><?php echo $row->koridor; ?></td>
            <td><?php echo $row->trayek; ?></td>
            <td><?php echo $row->rute; ?></td>
            
          </tr>
        <?php
            } if (mysqli_num_rows($query) == 0) {
                echo '<tr><td colspan="5" class="text-center">Tidak ada data.</td></tr>';
            }
        ?>
        </tbody>
      </table>

    </div>



    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>


    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>
  

</body>
<script>
$(document).ready(function() {
    $('.table').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Data Rute'
            },
            {
                extend: 'pdfHtml5',
                title: 'Data Rute'
            },
            {
                extend: 'print',
                title: 'Data Rute'
            }
        ]
    } );
} );
</script>

</html>