-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2022 at 08:34 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bst`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`username`, `password`) VALUES
('danu', '12345'),
('fadil', '12345'),
('febri', '12345'),
('mardi', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `angkot`
--

CREATE TABLE `angkot` (
  `id_angkot` int(11) NOT NULL,
  `no_angkot` varchar(11) NOT NULL,
  `trayek` varchar(50) NOT NULL,
  `rute` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `angkot`
--

INSERT INTO `angkot` (`id_angkot`, `no_angkot`, `trayek`, `rute`) VALUES
(1, '01', 'Cipinang Gading - Perum Yasminn', 'Cipinang Gading - Jl. Soemantadiredja - Cipaku - Jl.Pahlawan - Jl. Empang - Jl.Ir.H.Djuanda - Jl.Paledang - Jl. Kapten Muslihat - Jl. Veteran - Jl. Perintis Kemerdekaan - Terminal Merdeka - Jl. Mawar '),
(2, '02', 'Warung Nangka - BTM', 'Warung Nangka - Rancamaya - Detour Road / Cogreg - Jl. Cipaku - Jl. Pahlawan - Gg. Aut - BTM'),
(3, '03', 'Cimahpar - BTM', 'Cimahpar - Jl. Sancang - Jl. Kumbang - Jl. Lodaya - Jl. Raya Padjajaran - Jl. Otista - Jl. Ir. H. Juanda - BTM '),
(4, '04', 'Cimahpar - Warung Jambu ( Via Jl. A. Sobana )', 'Cimahpar - Jl. KH. Achmad Adnawijaya - Jl. A. Sobana - Jl. Gagalur - Jl. KH. Achmad Adnawijaya ( Jl. Pandu Raya ) - Warung Jambu'),
(5, '05', 'Ciheleut - Pasar Baru Bogor', 'Ciheuleut - Jl. Raya Pajajaran - Jl. Sambu - Baranangsiang - Jl. Otista - Pasar Bogor');

-- --------------------------------------------------------

--
-- Table structure for table `biskita`
--

CREATE TABLE `biskita` (
  `id_bis` int(11) NOT NULL,
  `koridor` int(11) NOT NULL,
  `trayek` varchar(50) NOT NULL,
  `rute` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `biskita`
--

INSERT INTO `biskita` (`id_bis`, `koridor`, `trayek`, `rute`) VALUES
(1, 1, 'Terminal Bubulak - Ciawi', 'Halte Terminal Bubulak - Halte SBJ 1 -Halte Semplak Halte Ruko Yasmin 1 -Halte Radar Bogor - Halte Transmart - Halte Cimanggu 1 -Halte UIKA 1'),
(2, 2, 'Terminal Bubulak - Cidangiang', 'Halte Terminal Bubulak - Halte Perum Sinbad -Halte BKP5K - Halte Lokatmala - Halte Pasar Gunung Batu - Halte Veteran - Halte BAPPEDA - Halte PMI - Halte Kebun Raya - Halte Cidangiang'),
(3, 5, 'Stasiun Bogor - Terminal Ciparigi', 'Stasiun Bogor - Bappeda - RS Salak - Sudirman - Air Mancur 2 - GOR - Gedung DPRD - SMPN 5 - Tugu Anti Narkoba 1 - Tugu Anti Narkoba 2 - Pemda 2 - SDN Kedung Halang 1 - Kedung Halang 2 - Villa Bogor Indah 2 - SMPN 19 Bogor 2 - Ciparigi.'),
(4, 6, 'Parung Banteng – Air Mancur', 'Parung Banteng - Parung Banteng 2 - BSI 2 - Simpang Bogor Baru - Bratasena - Taman Corat Coret 1 - Jambu Dua 1 - SLTPN 8 - BPJS -DinKes - Air Mancur 1');

-- --------------------------------------------------------

--
-- Table structure for table `kendaran`
--

CREATE TABLE `kendaran` (
  `id_kendaraan` varchar(5) NOT NULL,
  `jenis_kendaraan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kendaran`
--

INSERT INTO `kendaran` (`id_kendaraan`, `jenis_kendaraan`) VALUES
('A1', 'angkot'),
('B1', 'biskita');

-- --------------------------------------------------------

--
-- Table structure for table `rute`
--

CREATE TABLE `rute` (
  `id_rute` varchar(5) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `rute` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rute`
--

INSERT INTO `rute` (`id_rute`, `kode`, `rute`) VALUES
('RA01', '01', 'Cipinang Gading - Jl. Soemantadiredja - Cipaku - Jl.Pahlawan - Jl. Empang - Jl.Ir.H.Djuanda - Jl.Paledang - Jl. Kapten Muslihat - Jl. Veteran - Jl. Perintis Kemerdekaan - Terminal Merdeka - Jl. Mawar '),
('RB1', 'koridor 1', 'Halte Terminal Bubulak - Halte SBJ 1 -Halte Semplak Halte Ruko Yasmin 1 -Halte Radar Bogor - Halte Transmart - Halte Cimanggu 1 -Halte UIKA 1');

-- --------------------------------------------------------

--
-- Table structure for table `trayek`
--

CREATE TABLE `trayek` (
  `id_trayek` varchar(5) NOT NULL,
  `trayek` varchar(100) NOT NULL,
  `id_rute` varchar(5) NOT NULL,
  `id_kendaraan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trayek`
--

INSERT INTO `trayek` (`id_trayek`, `trayek`, `id_rute`, `id_kendaraan`) VALUES
('TA01', 'Cipinang Gading - Perum Yasmin', 'RA01', 'A1'),
('TB1', 'Terminal Bubulak - Ciawi', 'RB1', 'B1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `angkot`
--
ALTER TABLE `angkot`
  ADD PRIMARY KEY (`id_angkot`);

--
-- Indexes for table `biskita`
--
ALTER TABLE `biskita`
  ADD PRIMARY KEY (`id_bis`);

--
-- Indexes for table `kendaran`
--
ALTER TABLE `kendaran`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `rute`
--
ALTER TABLE `rute`
  ADD PRIMARY KEY (`id_rute`);

--
-- Indexes for table `trayek`
--
ALTER TABLE `trayek`
  ADD KEY `id_rute` (`id_rute`),
  ADD KEY `id_kendaraan` (`id_kendaraan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `angkot`
--
ALTER TABLE `angkot`
  MODIFY `id_angkot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `biskita`
--
ALTER TABLE `biskita`
  MODIFY `id_bis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `trayek`
--
ALTER TABLE `trayek`
  ADD CONSTRAINT `trayek_ibfk_1` FOREIGN KEY (`id_rute`) REFERENCES `rute` (`id_rute`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
