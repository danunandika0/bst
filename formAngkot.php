<?php 
    include 'connect.php';

    $act = 'add';

    if(!empty($_GET['id_angkot'])) {
        $sql = 'SELECT * FROM angkot WHERE id_angkot="'.$_GET['id_angkot'].'"';
        
        $query = mysqli_query($conn, $sql);
   
        if(mysqli_num_rows($query)) {
            $act = 'edit';

            $row = mysqli_fetch_object($query);
        }
    }
?>
 <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Bogor Smart Trans - Form Angkot</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<h1 class="mt-3 mb-3 container">Form Angkot</h1>
<form action="saveAngkot.php" method="POST" class="container" >

    <div class="mb-3">
        <label class="form-label">No.Angkot</label>
        <input type="text" class="form-control" name="no_angkot" autocomplete='off' placeholder="No.Angkot" value="<?php if ($act == 'edit') echo $row->no_angkot; ?>"
            required>
        <input type="hidden" name="id_angkot" value="<?php if ($act == 'edit') echo $row->id_angkot; ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Trayek</label>
        <input type="text" class="form-control" name="trayek" autocomplete='off' placeholder="Trayek" value="<?php if ($act == 'edit') echo $row->trayek; ?>"
            required>
        
    </div>
    <div class="mb-3">
        <label class="form-label">Rute</label>
        <input type="text" class="form-control" name="rute" autocomplete='off' placeholder="Rute" value="<?php if ($act == 'edit') echo $row->rute; ?>"
            required>
        
    </div>
    
    <div class="mb-3">
        <input type="submit" value="Simpan" class="btn btn-sm btn-success">
        <a href="angkot2.php" class="btn btn-sm btn-warning">Batal</a>
    </div>
</form>

<!-- Vendor JS Files -->
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>
